#pragma once
#include <windows.h>

//Temp addresses (for replacement in stub)
#define Tdata_section_rva 0x11111111
#define Timage_base 0x22222222

struct Basic_file_info
{
	DWORD original_number_of_sections;
	DWORD key_rva_minus_4c;
	DWORD total_virtual_size;
	DWORD total_size_of_raw_data;
	DWORD OEP;
	DWORD original_import_directory_rva;
	DWORD original_import_directory_size;
	DWORD original_resource_directory_rva;
	DWORD original_resource_directory_size;
	DWORD text_section_rva;
	BYTE key[32];
	DWORD first_dword_of_key;

	//IAT
	DWORD load_library_a_ptr; //Pointer to LoadLibraryA RVA [WITHOUT IMAGE_BASE]
	DWORD get_proc_address_ptr; //Pointer to GetProcAddress RVA [WITHOUT IMAGE_BASE]

	//TLS
	DWORD tls_index;
	DWORD original_tls_index_rva;
};