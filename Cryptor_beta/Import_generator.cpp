#include "Import_generator.h"

Import_generator::Import_generator()
{
	srand(GetTickCount());	
	//number_of_functions = (rand()%301)+300;
	number_of_functions = (rand()%104)+205;
	Get_Dlls();
}

void Import_generator::Get_Dlls()
{
	char system32_folder[MAX_PATH];

	//Get system32 directory path
	SHGetSpecialFolderPath(0,system32_folder, CSIDL_SYSTEM,true);
	string mask = system32_folder;
	string system32_folder_str = mask;
	mask += "\\*.dll";
	memcpy(system32_folder, &mask[0], sizeof(mask));

	//Get all *.dll files from System32 directory
	WIN32_FIND_DATA FindFileData;
	HANDLE hf;
	hf=FindFirstFile(system32_folder, &FindFileData);
	if (hf!=INVALID_HANDLE_VALUE)
	{
		do
		{
			string current_dll = system32_folder_str + "\\" + FindFileData.cFileName;
			ifstream pe_file(current_dll, std::ios::in | std::ios::binary);
			if (!pe_file) continue;			
			array_of_dlls.push_back(current_dll);
		}
		while (FindNextFile(hf,&FindFileData)!=0);
		FindClose(hf);
	}
}

bool Import_generator::Check_dll(string dll)
{
	ifstream pe_file(dll, std::ios::in | std::ios::binary);
	if(!pe_file) return false;
	
	try	
	{
		//Creating image using factory
		pe_base image(pe_factory::create_pe(pe_file));
		pe_file.close();

		if (image.has_exports()) return true;
		else return false;
	}

	catch(const pe_exception) {return false;}

}

string Import_generator::Get_random_dll()
{
	srand(GetTickCount());
	DWORD random_index = rand()%array_of_dlls.size();
	string current_dll = array_of_dlls[random_index];
	if (!Check_dll(current_dll) || Get_dll_name(current_dll) == "kernel32.dll")	//if current dll is kernel32, then get another dll;
	{
		//Remove dll from dll_list, because it doesn't have exports
		array_of_dlls.erase(array_of_dlls.begin() + random_index);
		Get_random_dll();
	}
	//Remove dll from dll_list because of double adding
	array_of_dlls.erase(array_of_dlls.begin() + random_index);
	return current_dll;
}

string Import_generator::Get_dll_name(string dll_path)
{
	string dir_name;
	string file_name;
	string::size_type slash_pos;
	if((slash_pos = dll_path.find_last_of("/\\")) != std::string::npos)
	{
		dir_name = dll_path.substr(0, slash_pos + 1); //Directory
		file_name = dll_path.substr(slash_pos + 1); //Filename
	}
	return file_name;
}

vector<string> Import_generator::Get_functions(string dll_path)
{
	//WinAPI functions list
	vector<string> WinAPI_list;

	ifstream pe_file(dll_path, std::ios::in | std::ios::binary);
	try	
	{
		//Creating image using factory
		pe_base image(pe_factory::create_pe(pe_file));
		pe_file.close();
		
		//Get Export information
		export_info info;
		const exported_functions_list exports = get_exported_functions(image, info);

		//For each function in dll
		for(exported_functions_list::const_iterator it = exports.begin(); it != exports.end(); ++it)
		{
			const exported_function& func = *it; //Exported function
			//We will add only functions, that have names
			if(func.has_name()) WinAPI_list.push_back(func.get_name());
		}
		
		return WinAPI_list;
 	}

	catch(const pe_exception) {return WinAPI_list;}
}

void Import_generator::Build_imports()
{
	//Load random dll, then load random
	//quantity of random WinAPI functions,
	//Push it back to imports
	while (true)
	{
		//Library name without path
		string dll_name;
		//Function list from library
		vector<string> function_list;
		//Loop, while function list is empty
		do 
		{
			string lib_path = Get_random_dll();		
			function_list = Get_functions(lib_path);
			dll_name = Get_dll_name(lib_path);
			if(!function_list.empty()) break;
		} while (true);
		
		import_library dll;
		dll.set_name(dll_name); //Set random dll name
	
		//If number of functions in dll < 10, then add them all
		if(function_list.size() < 50)
		{
			for(int i = 0; i != function_list.size(); i++)
			{
				//Add function to imports
				imported_function func;
				func.set_name(function_list[i]);
				dll.add_import(func);
	
				//Dec number of sections to add && return, if job is done
				number_of_functions--;
				if (!number_of_functions) return;
			}		 
		}
		//If number of functions in dll >= 10, then add random quantity of random WinAPIs
		else
		{
			srand(GetTickCount());
			int q = function_list.size();
			if (q > 103) q = 103;
			//Random quantity of WinAPI to add
			int quantity = (rand()%(q-49))+50;
	
			for (int i = 0; i!= quantity; i++)
			{
				//Random index for current function
				int rnd_index = rand()%function_list.size();
	
				//Add function to imports
				imported_function func;
				func.set_name(function_list[rnd_index]);
				dll.add_import(func);
	
				//Remove function from function list, because of double adding
				function_list.erase(function_list.begin() + rnd_index);
	
				//Dec number of sections to add && return, if job is done
				number_of_functions--;
				if (!number_of_functions) return;
			}
			
		}

		//Push current dll with random WinAPIs back to imports
		imports.push_back(dll);
	}
}

imported_functions_list Import_generator::Get_new_imports()
{	
	Build_imports();

	//Generate kernel32.dll
	import_library kernel32;
	kernel32.set_name("kernel32.dll");

	//Add LoadLibraryA and GetProcAddress to kernel32
	imported_function func;
	func.set_name("GetProcAddress");
	kernel32.add_import(func); 
	func.set_name("LoadLibraryA");
	kernel32.add_import(func);

	//Push kernel32 to random position in imports
	srand(GetTickCount());
	int rnd_pos = rand()%imports.size();
	imports.insert(imports.begin() + rnd_pos, kernel32);

	return imports;
}