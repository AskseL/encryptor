#pragma once
#include "includes.h"

class RC4
{
public:
	//Creates RC4 with key size = 32bytes
	RC4();
	//~RC4(void);
	//Crypt it!
	void encrypt(std::string& data);
	byte* get_key_ptr();
private:
	uint32_t key_size;
	byte key[32];
	byte S[256];
	uint32_t i,j;

	//Key-Scheduling Algorithm(KSA)(S buffer generation)
	void rc4_init(const byte key[], uint32_t key_length);
	//Returns byte for XOR
	byte get_byte();	
};