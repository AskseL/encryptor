#include "RC4.h"
#include <dos.h>

RC4::RC4()
{
	key_size = 32;	
	srand(GetTickCount());	
	Sleep(10);

	for (uint32_t i = 0; i < key_size; i++)
	{		
		key[i] = rand();
	}
	rc4_init(key, key_size);
}

void RC4::rc4_init(const byte key[], uint32_t key_length)
{
	byte temp;

	for( i = 0; i != 256; ++i )
		S[ i ] = i;

	for( i = j = 0; i != 256; ++i )
	{
		j = ( j + key[ i % key_length ] + S[ i ] ) % 256;
		temp = S[ i ];
		S[ i ] = S[ j ];
		S[ j ] = temp;
	}

	i = j = 0;
}

byte RC4::get_byte()
{
	byte temp;

	i = ( i + 1 ) % 256;
	j = ( j + S[ i ] ) % 256;

	temp = S[ j ];
	S[ j ] = S[ i ];
	S[ i ] = temp;

	return S[ ( temp + S[ j ] ) % 256 ];
}

void RC4::encrypt(std::string& data)
{
	for (uint32_t i = 0; i < data.length(); i++)
	{
		data[i]^=get_byte();
	}
}

byte* RC4::get_key_ptr()
{
	return key;
}