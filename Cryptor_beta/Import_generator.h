#pragma once
#include "includes.h"
#include "shlobj.h"
#pragma comment(lib, "D:/pe_bliss_1.0.0/Debug/pe_bliss.lib")

using namespace std;
using namespace pe_bliss;

class Import_generator
{
public:
	//Creates Import generator with random number of WinAPI functions
	Import_generator();

	//Returns new auto-generated import table with random quantity of random WinAPI functions
	imported_functions_list Get_new_imports();

private:		
	DWORD number_of_functions;
	vector<string> array_of_dlls;
	imported_functions_list imports;

	//Returns array of all dlls in system32 folder
	void Get_Dlls();
	//Returns true, if dll has exported functions
	bool Check_dll(string);
	//Returns random checked dll
	string Get_random_dll();
	//Cut the dll's name from the path
	string Get_dll_name(string);
	//Get WinAPI list from dll
	vector<string> Get_functions(string);
	//Build Imports
	void Build_imports();

};