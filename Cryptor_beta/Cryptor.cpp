#include "includes.h"
#include "structs.h"
#include "RC4.h"
#include "Import_generator.h"
#include "stub.h"

#pragma comment(lib, "D:/pe_bliss_1.0.0/Debug/pe_bliss.lib")

using namespace std;
using namespace pe_bliss;

const string filename = "D:\\projects\\Cryptor_beta\\test\\2.exe";
const string filename2 = "D:\\projects\\Cryptor_beta\\test\\ollydbg.exe";


//Returns value offset in generated stub
DWORD find_offset_in_stub(DWORD mask)
{
	string substr = string(reinterpret_cast<const char*>(&mask), sizeof(DWORD));
	string str = string(reinterpret_cast<const char*>(generated_stub), sizeof(generated_stub));

	size_t pos = str.find(substr);
	if (pos != std::string::npos)
		return pos;
	else
		return -1;
}

DWORD data_section_rva_offset = find_offset_in_stub(Tdata_section_rva);
DWORD image_base_offset = find_offset_in_stub(Timage_base);

int main()
{	
	#pragma region Trying to open file
	ifstream pe_file(filename, std::ios::in | std::ios::binary);
	if(!pe_file)
	{ 
		std::cout << "[!] Cannot open " << filename << std::endl;
		_getch();
		return -1;
	}
    #pragma endregion

	//Main code//
	try	
	{
		//Creating image using factory
		pe_base image(pe_factory::create_pe(pe_file));
		pe_file.close();
		
		cout << "[+] Entropy: " << entropy_calculator::calculate_entropy(image) << endl << endl;


		//Fill Basic File Info structure
		Basic_file_info basic_file_info = {0};
		basic_file_info.original_number_of_sections = image.get_number_of_sections();
		basic_file_info.OEP = image.get_ep();
		basic_file_info.original_import_directory_rva = image.get_directory_rva(IMAGE_DIRECTORY_ENTRY_IMPORT);
		basic_file_info.original_import_directory_size = image.get_directory_size(IMAGE_DIRECTORY_ENTRY_IMPORT);
		basic_file_info.original_resource_directory_rva = image.get_directory_rva(IMAGE_DIRECTORY_ENTRY_RESOURCE);
		basic_file_info.original_resource_directory_size = image.get_directory_size(IMAGE_DIRECTORY_ENTRY_RESOURCE);

		
		#pragma region Get Section list

		section_list& sl = image.get_image_sections();		
		if (sl.empty())
		{
			cout << "[!] The file doesn't have sections" << endl;
			_getch();
			return -1;
		}
		#pragma endregion


		#pragma region Create new resources

		//����� ������ �������� ���������� ��������
		resource_directory new_root_dir;

		#pragma region Trying to open file
		ifstream pe_file2(filename2, std::ios::in | std::ios::binary);
		if(!pe_file)
		{ 
			std::cout << "[!] Cannot open " << filename2 << std::endl;
			_getch();
			return -1;
		}
		#pragma endregion		
		
		//Creating image using factory
		pe_base image2(pe_factory::create_pe(pe_file2));
		pe_file2.close();


		if(image2.has_resources())
		{
			std::cout << "Creating new resources..." << std::endl;

			//������� ������� ��������� ����� (�������� ����������)
			resource_directory root_dir = get_resources(image2);
			//����������� ������������ � ����� ���������� ��������
			//�� ��������������� ������
			pe_resource_viewer res(root_dir);
			pe_resource_manager new_res(new_root_dir);

			try
			{
				//���������� ��� ����������� ������ ������
				//� ������ ������, ������� ID
				pe_resource_viewer::resource_id_list icon_id_list(res.list_resource_ids(pe_resource_viewer::resource_icon_group));
				pe_resource_viewer::resource_name_list icon_name_list(res.list_resource_names(pe_resource_viewer::resource_icon_group));
				//������� ������ ������������� ����������� �������, ������� ��������, ���� �� ���
				if(!icon_name_list.empty())
				{
					//������� ����� ������ ������ ��� ������ ������� ����� (�� ������� 0)
					//���� ���� ���� �� ����������� ����� ��� �������� ������, ����� ���� ������� list_resource_languages
					//���� ���� ���� �� �������� ������ ��� ����������� �����, ����� ���� ������� get_icon_by_name (���������� � ��������� �����)
					//������� ������ ������ � ����� ���������� ��������
					resource_cursor_icon_writer(new_res).add_icon(
						resource_cursor_icon_reader(res).get_icon_by_name(icon_name_list[0]),
						icon_name_list[0],
						res.list_resource_languages(pe_resource_viewer::resource_icon_group, icon_name_list[0]).at(0));
				}
				else if(!icon_id_list.empty()) //���� ��� ����������� ����� ������, �� ���� ������ � ID
				{
					//������� ����� ������ ������ ��� ������ ������� ����� (�� ������� 0)
					//���� ���� ���� �� ����������� ����� ��� �������� ������, ����� ���� ������� list_resource_languages
					//���� ���� ���� �� �������� ������ ��� ����������� �����, ����� ���� ������� get_icon_by_id_lang
					//������� ������ ������ � ����� ���������� ��������
					resource_cursor_icon_writer(new_res).add_icon(
						resource_cursor_icon_reader(res).get_icon_by_id(icon_id_list[0]),
						icon_id_list[0],
						res.list_resource_languages(pe_resource_viewer::resource_icon_group, icon_id_list[0]).at(0));
				}
			}
			catch(const pe_exception&)
			{
				//���� �����-�� ������ � ���������, ��������, ������ ���,
				//�� ������ �� ������
			}

			try
			{
				//������� ������ ����������, ������� ID
				pe_resource_viewer::resource_id_list manifest_id_list(res.list_resource_ids(pe_resource_viewer::resource_manifest));
				if(!manifest_id_list.empty()) //���� �������� ����
				{
					//������� ����� ������ �������� ��� ������ ������� ����� (�� ������� 0)
					//������� �������� � ����� ���������� ��������
					new_res.add_resource(
						res.get_resource_data_by_id(pe_resource_viewer::resource_manifest, manifest_id_list[0]).get_data(),
						pe_resource_viewer::resource_manifest,
						manifest_id_list[0],
						res.list_resource_languages(pe_resource_viewer::resource_manifest, manifest_id_list[0]).at(0)
						);
				}
			}
			catch(const pe_exception&)
			{
				//���� �����-�� ������ � ���������,
				//�� ������ �� ������
			}

			try
			{
				//������� ������ �������� ���������� � ������, ������� ID
				pe_resource_viewer::resource_id_list version_info_id_list(res.list_resource_ids(pe_resource_viewer::resource_version));
				if(!version_info_id_list.empty()) //���� ���������� � ������ ����
				{
					//������� ����� ������ ��������� ���������� � ������ ��� ������ ������� ����� (�� ������� 0)
					//������� ���������� � ������ � ����� ���������� ��������
					new_res.add_resource(
						res.get_resource_data_by_id(pe_resource_viewer::resource_version, version_info_id_list[0]).get_data(),
						pe_resource_viewer::resource_version,
						version_info_id_list[0],
						res.list_resource_languages(pe_resource_viewer::resource_version, version_info_id_list[0]).at(0)
						);
				}
			}
			catch(const pe_exception&)
			{
				//���� �����-�� ������ � ���������,
				//�� ������ �� ������
			}
		}

		#pragma endregion


		#pragma region Get TLS Info

		std::auto_ptr<tls_info> tls;
		if(image.has_tls())
		{
			std::cout << "Reading TLS..." << std::endl;
			tls.reset(new tls_info(get_tls_info(image)));
		}
		if(tls.get()) basic_file_info.original_tls_index_rva = tls->get_index_rva();

		#pragma endregion


		#pragma region Rebuild section list

			//String for all sections data
			string sections_data_raw = "";
			//String for all sections headers
			string sections_headers_raw = "";

			//Merge sections data and headers together
			for (int i = 0; i != image.get_number_of_sections(); i++)
			{
				const section& sec = sl[i];

				pe_win::image_section_header hdr = sec.get_raw_header();
				sections_headers_raw += string(reinterpret_cast<char*>(&hdr), sizeof(pe_win::image_section_header));
				sections_data_raw += sec.get_raw_data();
			}

			//If all sections are empty
			if (sections_data_raw.empty())
			{
				cout << "[!] All sections are empty" << endl;
				_getch();
				return -1;
			}

			cout << "Rebuilding file sections.." << endl;

			//Encrypt all data raw
			RC4 encryptor;
			encryptor.encrypt(sections_headers_raw);
			encryptor.encrypt(sections_data_raw);
			//Copy key to basic info
			memcpy(&basic_file_info.key, encryptor.get_key_ptr(), 32);

			//Copy first 4 bytes of key to basic info for antiemulation
			memcpy(&basic_file_info.first_dword_of_key, encryptor.get_key_ptr(), 4);
			//Fill original first 4 bytes of key with zeros
			memset(basic_file_info.key, 0, 4);

			//Remember total sections virtual size
			basic_file_info.total_virtual_size = image.get_size_of_image() - sl[0].get_virtual_address();
			//Remember total size of raw data
			basic_file_info.total_size_of_raw_data = sections_data_raw.length();
			//Delete all sections
			sl.clear();
			//Set minimal file alignment
			image.set_file_alignment(0x200);

			//Create new .bss section
			section bss_section;
			bss_section.set_name(".textbss");
			bss_section.readable(true).writeable(true).executable(false);
			bss_section.set_virtual_size(basic_file_info.total_virtual_size);
			section& added_bss_section = image.add_section(bss_section);

			//Create new .text section
			section text_section;
			text_section.set_name(".text");
			text_section.readable(true).writeable(false).executable(true);
			text_section.set_raw_data(sections_data_raw +
				string(reinterpret_cast<char*>(generated_stub), sizeof(generated_stub)));
			section& added_text_section = image.add_section(text_section);
			basic_file_info.text_section_rva = added_text_section.get_virtual_address();
			//added_text_section.get_raw_data().resize(added_text_section.get_raw_data().length()*2);
			//Set new Entry Point
			image.set_ep(added_text_section.get_virtual_address() + basic_file_info.total_size_of_raw_data);
			//Pointer to stub's first byte
			BYTE* stub_first_byte_ptr = reinterpret_cast<BYTE*>(&added_text_section.get_raw_data()[basic_file_info.total_size_of_raw_data]);
			//Pointers to data section RVA and image base in generated stub
			DWORD* data_section_rva_ptr = reinterpret_cast<DWORD*>(stub_first_byte_ptr + data_section_rva_offset);
			DWORD* image_base_prt = reinterpret_cast<DWORD*>(stub_first_byte_ptr + image_base_offset);

			//Create new .data section
			section data_section;
			data_section.set_name(".data");
			data_section.readable(true).writeable(true).executable(false);
			data_section.set_raw_data(string(reinterpret_cast<char*>(&basic_file_info), sizeof(Basic_file_info)) +
				sections_headers_raw);
			section& added_data_section = image.add_section(data_section);

			//Save pointer to key - 4C (for antiemulation)
			Basic_file_info* basic_file_info_ptr = reinterpret_cast<Basic_file_info*>(&added_data_section.get_raw_data()[0]);
			basic_file_info_ptr->key_rva_minus_4c = pe_base::rva_from_section_offset(added_data_section,
				offsetof(Basic_file_info, key)) - 0x4C;

			//Fill addresses in generated stub (IMAGE_BASE && DATA_SECTION_RVA)
			*data_section_rva_ptr = added_data_section.get_virtual_address();
			*image_base_prt = image.get_image_base_32();


			//Create new .idata section
			section idata_section;
			idata_section.set_name(".idata");
			idata_section.readable(true).writeable(true).executable(false);
			idata_section.set_raw_data("imports");
			section& added_idata_section = image.add_section(idata_section);

		#pragma endregion

		
		#pragma region Create new IAT

		cout << "Creating new imports.." << endl;

		//Automatically generate new imports with random quantity of random WinAPIs
		Import_generator import_generator;
		imported_functions_list imports = import_generator.Get_new_imports();

		//�������� ����������� ��������
		import_rebuilder_settings settings;
		settings.build_original_iat(false);
		settings.save_iat_and_original_iat_rvas(false, false);
		settings.enable_auto_strip_last_section(false);
		settings.rewrite_iat_and_original_iat_contents();

		//Rebuild imports
		rebuild_imports(image, imports, added_idata_section, settings);	

		#pragma endregion


		#pragma region Get pointers to LoadLibraryA and GetProcAddress WinAPI functions

		//Get pointers to LoadLibraryA and GetProcAddress WinAPI functions
		imports = get_imported_functions(image);
		for (imported_functions_list::const_iterator it = imports.begin(); it != imports.end(); ++it)
		{				
			int LoadLibraryA_index, GetProcAddress_index = 0;
			const import_library& lib = *it;
			if (lib.get_name() == "kernel32.dll")
			{
				const import_library::imported_list& functions = lib.get_imported_functions();
				int itr = 0;
				for(import_library::imported_list::const_iterator func_it = functions.begin(); func_it != functions.end(); ++func_it)
				{
					const imported_function& func = *func_it;
					if (func.get_name() == "LoadLibraryA") LoadLibraryA_index = itr;
					if (func.get_name() == "GetProcAddress") GetProcAddress_index = itr;
					itr++;
				}			
				Basic_file_info* info = reinterpret_cast<Basic_file_info*>(&added_data_section.get_raw_data()[0]);
				info->load_library_a_ptr = lib.get_rva_to_iat() + sizeof(DWORD)*LoadLibraryA_index;
				info->get_proc_address_ptr = lib.get_rva_to_iat() + sizeof(DWORD)*GetProcAddress_index;
				break;
			}

		}
		#pragma endregion
				
		
		#pragma region Rebuild TLS

		//���� � ����� ���� TLS
		if(tls.get())
		{		
			//Create new .tls section
			section tls_section;
			tls_section.set_name(".tls");
			tls_section.readable(true).writeable(true).executable(false);
			tls_section.set_raw_data("tls");
			section& added_tls_section = image.add_section(tls_section);

			//Set new TLS index RVA
			tls->set_index_rva(pe_base::rva_from_section_offset(added_data_section, offsetof(Basic_file_info, tls_index)));

			std::cout << "Rebuilding TLS..." << std::endl;

			//�� ������ ������ ������� ����� ���������
			tls->set_callbacks_rva(0);
			//������� ������ ���������
			tls->clear_tls_callbacks();

			//��������� ����� ������������� �����
			//������ ��� ������������� ��������� ������ ������
			tls->set_raw_data_start_rva(pe_base::rva_from_section_offset(added_tls_section, 0));
			//������������� ����� ����� ���� ������
			tls->recalc_raw_data_end_rva();

			//������������ TLS
			//��������� ������������, ��� �� ����� ������ ������ � ��������
			//�� ������� ��� ������� (�������� ��� ��������, ���� ����)
			//����� ���������, ��� �� ����� �������� ������� ����� � ����� ������
			rebuild_tls(image, *tls, added_tls_section, 0, false, false, tls_data_expand_raw, true, false);

			//��������� ������ ������� ��� �������������
			//��������� ������ ������
			added_tls_section.get_raw_data() += tls->get_raw_data();
			//������ ��������� ����������� ������ ������ "kaimi.ru"
			//� ������ SizeOfZeroFill ���� TLS
			image.set_section_virtual_size(added_tls_section, added_tls_section.get_size_of_raw_data() + tls->get_size_of_zero_fill());
			//�������, ������� ��� �������� ������� ����� � ����� ������
			//� ����������� �� ������� (���������� � �����������)
			image.prepare_section(added_tls_section);
		}

	#pragma endregion
		

		#pragma region Rebuild Resources

		if(!new_root_dir.get_entry_list().empty())
		{
			//Create new .rsrc section
			section rsrc_section;
			rsrc_section.set_name(".rsrc");
			rsrc_section.readable(true).writeable(false).executable(false);
			rsrc_section.set_raw_data("rsrc");
			section& added_rsrc_section = image.add_section(rsrc_section);

			cout << "Rebuilding resources.." << endl;

			//Rebuild resources
			rebuild_resources(image, new_root_dir, added_rsrc_section, 0, true, false);
		}
		#pragma endregion


		#pragma region Remove directories

		image.remove_directory(IMAGE_DIRECTORY_ENTRY_BASERELOC);
		image.remove_directory(IMAGE_DIRECTORY_ENTRY_BOUND_IMPORT);
		image.remove_directory(IMAGE_DIRECTORY_ENTRY_DELAY_IMPORT);
		image.remove_directory(IMAGE_DIRECTORY_ENTRY_EXPORT);
		image.remove_directory(IMAGE_DIRECTORY_ENTRY_IAT);
		image.remove_directory(IMAGE_DIRECTORY_ENTRY_LOAD_CONFIG);
		//image.remove_directory(IMAGE_DIRECTORY_ENTRY_RESOURCE);
		image.remove_directory(IMAGE_DIRECTORY_ENTRY_SECURITY);
		//image.remove_directory(IMAGE_DIRECTORY_ENTRY_TLS);
		image.remove_directory(IMAGE_DIRECTORY_ENTRY_DEBUG);

		#pragma endregion


		#pragma region Rebuild and save image

		//������������ � ��������� �����
		std::string base_file_name;		
		//������� ����� PE-����
		//�������� ��� ����������� ��� ����� ��� ����������
		base_file_name = filename;
		std::string dir_name;
		std::string::size_type slash_pos;
		if((slash_pos = base_file_name.find_last_of("/\\")) != std::string::npos)
		{
			dir_name = base_file_name.substr(0, slash_pos + 1); //���������� ��������� �����
			base_file_name = base_file_name.substr(slash_pos + 1); //��� ��������� �����
		}
		//����� ������ ����� ��� packed_ + ���_�������������_�����
		//������ � ���� �������� ����������, ����� ���������
		//���� ����, ��� ����� ��������
		base_file_name = dir_name + "result.exe";

		//�������� ����
		std::ofstream new_pe_file(base_file_name.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
		if(!new_pe_file)
		{
			//���� �� ������� ������� ���� - ������� ������
			std::cout << "Cannot create " << base_file_name << std::endl;
			return -1;
		}
		//������������ PE-�����
		//������� DOS-���������, ���������� �� ���� NT-���������
		//(�� ��� �������� ������ �������� true)
		//�� ������������� SizeOfHeaders - �� ��� �������� ������ ��������
		rebuild_pe(image, new_pe_file, false, false);

		#pragma endregion	

		//��������� ������������, ��� ���� �������� �������
		cout << endl << "[+] Successfully rebuilded!" << endl;
		std::cout << "[+] New Entropy: " << entropy_calculator::calculate_entropy(image) << std::endl;
		std::cout << "[+] Saved as: " << base_file_name << std::endl;			
	}

	#pragma region Exceptions

	catch(const pe_exception& e)
	{
		//���� �������� ������
		std::cout << "[!] Error: " << e.what() << std::endl;
		_getch();
		return -1;
	}

	#pragma endregion

	_getch();
	return 0;
}