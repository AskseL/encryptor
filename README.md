```
#!c++

This is the PE binary file encryptor. It uses RC4 cipher. Works with x32 executable binary.
Supports:
[+] Imports
[+] Resources
[+] TLS
[+] Relocations
[-] Exports
[-] Load Config

```