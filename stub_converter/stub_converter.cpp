#include "../Cryptor_beta/includes.h"

#pragma comment(lib, "D:/pe_bliss_1.0.0/Debug/pe_bliss.lib")
using namespace std;
using namespace pe_bliss;

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		std::cout << "Usage: unpacker_converter.exe unpacker.exe output.h" << std::endl;
		_getch();
		return 0;
	}

	std::ifstream file(argv[1], std::ios::in | std::ios::binary);
	if(!file)
	{
		//���� ������� ���� �� ������� - ������� � ������ � �������
		std::cout << "Cannot open " << argv[1] << std::endl;
		_getch();
		return -1;
	}

	try
	{
		std::cout << "Creating unpacker source file..." << std::endl;

		pe_base image(pe_factory::create_pe(file));

		//�������� ������ ������ �����
		section_list& stub_sections = image.get_image_sections();
		//��������, ��� ��� ���� (��� ��� � ��� ��� ��������, ���������)
		if(stub_sections.size() != 1)
		{
			std::cout << "Incorrect stub" << std::endl;
			_getch();
			return -1;
		}

		//�������� ������ �� ������ ���� ������
		std::string& stub_section_data = stub_sections.at(0).get_raw_data();
		//������� ������� ����� � ����� ���� ������,
		//������� ���������� ������� ��� ������������		
		pe_utils::strip_nullbytes(stub_section_data);

		//��������� �������� ���� ��� ������ h-�����
		//��� ��� �������� � argv[2]
		std::ofstream output_source(argv[2], std::ios::out | std::ios::trunc);

		//�������� ����������� �������� ���
		output_source << std::hex << "#pragma once" << std::endl << "unsigned char generated_stub[] = {";
		//������� ����� ��������� ������
		unsigned long len = 0;
		//����� ����� ������ ������
		//std::string::size_type total_len = stub_section_data.length();
		std::string::size_type total_len = stub_section_data.length();

		//��� ������� ����� ������...
		for(std::string::const_iterator it = stub_section_data.begin(); it != stub_section_data.end(); ++it, ++len)
		{
			//��������� ����������� ��������, �����
			//������������ ��� ��� ��������
			if((len % 16) == 0)
				output_source << std::endl;

			//���������� �������� �����
			output_source
				<< "0x" << std::setw(2) << std::setfill('0')
				<< static_cast<unsigned long>(static_cast<unsigned char>(*it));

			//�, ���� ����������, �������
			if(len != total_len - 1)
				output_source << ", ";
		}

		//����� ����
		output_source << " };" << std::endl;
	}
	catch(const pe_exception& e)
	{
		//���� �� �����-�� ������� ������� ��� �� �������
		//������� ����� ������ � ������
		std::cout << e.what() << std::endl;
		_getch();
		return -1;
	}

	return 0;
}